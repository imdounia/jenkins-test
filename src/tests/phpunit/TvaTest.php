<?php

use PHPUnit\Framework\TestCase;

use Formation\CalculTvaTest;

class TvaTest extends TestCase
{
    public function testCalculTva()
    {
       
        $this->assertEquals(120, calculerTVA(100, 20));
        
        
        $this->assertEquals(55, calculerTVA(50, 10));
        
        
        $this->assertEquals(250, calculerTVA(200, 25));
    }
}