<?php

namespace Formation;

function calculerTVA($montantHT, $tauxTVA) {
    // Calcul de la TVA
    $tva = $montantHT * ($tauxTVA / 100);

    $montantTTC = $montantHT + $tva;

    return $montantTTC;
}

// Exemple d'utilisation de la fonction
$montantHT = 100; 
$tauxTVA = 20; 

echo "Montant TTC : " . calculerTVA($montantHT, $tauxTVA);